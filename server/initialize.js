import Product from  './product/productModel.js';

let init = () => {
  // (req, res) => {
  let initialState = {};

  let sendInitialState = () => {
    Product.findAll()
      .then( (products) => {
        initialState = {
          product: {
            family: products.family,
            edition: products.edition,
            license: products.license,
          }
        };
        console.log(initialState);
        //res.send(initialState);
      })
      .catch( (error) => {
        console.log('error in initialize.js');
        //res.send(initialState);
      });
  };
  sendInitialState();
};

export default init;
