import db from '../database/db.js';
import Product from '../product/productModel.js'
import formidable from 'formidable';
import fs from 'fs';

const UPLOADS_PATH = __dirname + "/uploads/";

/* Reason for using raw sql query:
 * Sequelize doesn't have an api for LOAD DATA INFILE so must use raw query instead.
 * Table is also being created using a raw query because the order of column
 * creation can't be specified in sequelize. Import fails if the id and created_at
 * columns are created first, as they are using the sequelize api. Using raw
 * query as below ensures stable import based on changing # of columns.
 */
let uploadCsvToDatabase = (filePath) => {
  // Query Strings:
  const dropProductTable = "DROP TABLE IF EXISTS product;";
  const createProductTable =
    "CREATE TABLE product " +
    "(" +
    "description TEXT DEFAULT NULL, " +
    "comments TEXT DEFAULT NULL, " +
    "sku TEXT DEFAULT NULL, " +
    "pack_price TEXT DEFAULT NULL, " +
    "family VARCHAR(255) DEFAULT NULL, " +
    "edition TEXT DEFAULT NULL, " +
    "licensing_type TEXT DEFAULT NULL, " +
    "pack_size TEXT DEFAULT NULL, " +
    "sku_notes TEXT DEFAULT NULL, " +
    "sku_type TEXT DEFAULT NULL, " +
    "premise_cloud TEXT DEFAULT NULL, " +
    "sns_type TEXT DEFAULT NULL, " +
    "sns_length TEXT DEFAULT NULL, " +
    "sector TEXT DEFAULT NULL, " +
    "eur TEXT DEFAULT NULL, " +
    "gbp TEXT DEFAULT NULL, " +
    "jpy TEXT DEFAULT NULL, " +
    "aud TEXT DEFAULT NULL, " +
    "cny TEXT DEFAULT NULL, " +
    "emea_usd TEXT DEFAULT NULL, " +
    "usd_g TEXT DEFAULT NULL" +
    ") ";
  const loadDataFromCSV = "LOAD DATA INFILE " + "'" + filePath + "'" + " INTO TABLE product COLUMNS TERMINATED BY ',' ENCLOSED BY '\"'";
  const createPrimaryKey = "ALTER TABLE product ADD COLUMN id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ";
  const createTimeStamps = "ALTER TABLE product ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP";

  // TODO: Use map to avoid promise hell here.
  // Async issues occur unless .then's follow db.query calls as below
  return db.query(dropProductTable)
      .then(
        () => {
          return db.query(createProductTable)
        .then(
          () => {
            return db.query(loadDataFromCSV)
          .then(
            () => {
              return db.query(createPrimaryKey)
            .then(
              () => {
                return db.query(createTimeStamps);
            })
          })
        })
      })
    .catch( err => {
      console.error("ERROR: master csv file could not be written to the database using raw sql query.");
      throw err;
    });
};


let copyFile = (source, target, cb) => {
  let cbCalled = false;
  let rd = fs.createReadStream(source);
  rd.on("error", (err) => {
    done(err);
  });
  let wr = fs.createWriteStream(target);
  wr.on("error", (err) => {
    done(err);
  });
  wr.on("close", (ex) => {
    done();
  });
  rd.pipe(wr);
  let done = (err) => {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  };
};
// Handle saving the incoming csv file and then upload the file contents to mysql
// Revert to the previously uploaded file if the upload fails.
let masterCsv = (req, res) => {
  let form = new formidable.IncomingForm();
  let fileName = "";
  let currentFile = "";
  let previousFile = UPLOADS_PATH + 'previous.csv';
  form.encoding = 'utf-8';
  form.parse(req);
  form.on('progress', (bytesReceived, bytesExpected) => {
    // TODO: Consider implementing progress bar on front end. Fires every time a chunk is processed.
    // e.g. console.log("So far, ", bytesReceived, "have been received and ", bytesExpected, "are still expected.");
  });
  form.on('fileBegin', (name, file) => {
    currentFile = file.path = UPLOADS_PATH + file.name;
  });
  form.on('file', (name, file) => {
    fileName = file.name;
    console.log('Uploaded ' + file.name);
  });
  form.on('error', (err) => {
    // TODO: Be more specific about error handling on front end for 400's
    res.status(400);
  });
  form.on('end', () => {
    uploadCsvToDatabase(currentFile)
      .then(
        () => {
          // TODO: Update so that prepared data gets sent down rather than all products
          Product.findAll()
            .then( products => {
              res.status(202).json(products);
              copyFile(currentFile, previousFile, (err) => {
                if (err) { console.error("ERROR: copyFile function failed in master.", err); }
              });
            });
        },
        // If the new file fails to upload, revert to the previous CSV file version
        (err) => {
          uploadCsvToDatabase(previousFile)
            .then(
              () => {
                console.error("ERROR: Reverting to previous CSV file.\n" +
                  "\nError Code: ", err.original.errno,
                  "\nError Message: ", err.original.message,
                  "\nRelated Query: ", err.original.sql);
                Product.findAll()
                  .then( products => {
                    res.status(203).json(products);
                  });
              })
              .catch( err => {
                // TODO: Get more specific about 400 errors on front end
                res.sendStatus(400);
                throw err;
              });
        });
  });
};

Product.sync()
  .catch(
    (err) => {
      console.error("ERROR: sync call in the csv/master failed.", err)
    });

export default masterCsv;
