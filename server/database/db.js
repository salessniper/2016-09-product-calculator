import Sequelize from 'sequelize';
import password from '../config/mysqlsetup.js'; //__dirname +   prefix might be needed

let db = new Sequelize('vmw', 'root', password, {
  host: 'localhost',
  dialect: 'mysql',
  define: {timestamps: false}
});

export default db;