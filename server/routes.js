import path from 'path';
import express from 'express';
import { getInitialState } from './product/prep.js';
import { preparedData } from './product/prep.js';
import productController from'./product/productController.js';
import masterCsv from  './csv/master';

let router = (app, express) => {
  console.log(__dirname.slice(0, -7) + '/dist/');
  app.use(express.static(__dirname.slice(0, -7) + '/dist'));
  app.get('/', getInitialState);
  app.get('/csv/master', masterCsv);
  app.post('/csv/master', masterCsv);
  // app.get('/product/prep', preparedData);
  app.post('/product/prep', preparedData)
};

export default router;