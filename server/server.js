import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import router from './routes';
import db from './database/index.js';
//import webpack from 'webpack';
//import webpackConfig from '../webpack.config.dev';
//import config from '../tools/default.json';
//import webpackDevMiddleware from 'webpack-dev-middleware';
//import webpackHotMiddleware from 'webpack-hot-middleware';
//import httpProxy from 'http-proxy';
//import bundle from './bundle.js';

//connects the database
db();
let app = express();
let port = process.env.PORT || 8080;
//let proxy = httpProxy.createProxyServer();
//let compiler = webpack(webpackConfig);
//let isProduction = process.env.NODE_ENV === 'production';
//let publicPath = path.resolve(__dirname, 'dist');

app.use(morgan('dev')); // dev use only
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// TODO:  Implement hot middleware, involves stripping starter-kit
//app.use(webpackDevMiddleware(compiler, {
//  publicPath: webpackConfig.output.publicPath || '/',
//  stats: { colors: true }
//}));
//
//app.use(webpackHotMiddleware(compiler, {
//  log: console.log
//}));

//if (config.env === 'development') {
//  app.use(webpackDevMiddleware(compiler, {
//    publicPath: webpackConfig.output.publicPath || '/'
//  }));
//} else {
//  app.use(staticFiles('public', {
//    dotfile: 'ignore',
//    etag: true,
//    index: false,
//    lastModified: true
//  }));
//}

// app.use(express.static(publicPath));

//if (config.env === 'development') {
//  // We require the bundler inside the if block because
//  // it is only needed in a development environment. Later
//  // you will see why this is a good idea
//
//  bundle();
//
//  // Any requests to localhost:3000/build is proxied
//  // to webpack-dev-server
//  app.all('/build/*', function (req, res) {
//    proxy.web(req, res, {
//      target: 'http://localhost:8080'
//    });
//  });
//}
//
//proxy.on('error', function(e) {
//  console.log('Could not connect to proxy, please try again...');
//});

router(app, express);

app.listen(port, function(err) {
  if(err) {
    return console.log(err);
  }
  console.log('Sales Sniper is listening on port: ' + port);
});


