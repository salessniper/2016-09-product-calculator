import  Sequelize from 'sequelize';
import db from '../database/db.js';

var Product = db.define('product',
  {
    description: {
      type: Sequelize.TEXT,
      field: 'description',
      length: 'MEDIUM'
    },
    comments: {
      type: Sequelize.TEXT,
      field: 'comments',
      length: 'MEDIUM'
    },
    sku: {
      type: Sequelize.STRING,
      field: 'sku'
    },
    pack_price : {
      type: Sequelize.STRING,
      field: 'pack_price'
    },
    family: {
      type: Sequelize.STRING,
      field: 'family'
    },
    edition: {
      type: Sequelize.STRING,
      field: 'edition'
    },
    licensing_type: {
      type: Sequelize.STRING,
      field: 'licensing_type'
    },
    pack_size: {
      type: Sequelize.STRING,
      field: 'pack_size'
    },
    sku_notes: {
      type: Sequelize.STRING,
      field: 'sku_notes'
    },
    sku_type: {
      type: Sequelize.STRING,
      field: 'sku_type'
    },
    premise_cloud: {
      type: Sequelize.STRING,
      field: 'premise_cloud'
    },
    sns_type: {
      type: Sequelize.STRING,
      field: 'sns_type'
    },
    sns_length: {
      type: Sequelize.STRING,
      field: 'sns_length'
    },
    sector: {
      type: Sequelize.STRING,
      field: 'sector'
    },
    eur: {
      type: Sequelize.STRING,
      field: 'eur'
    },
    gbp: {
      type: Sequelize.STRING,
      field: 'gbp'
    },
    jpy: {
      type: Sequelize.STRING,
      field: 'jpy'
    },
    aud: {
      type: Sequelize.STRING,
      field: 'aud'
    },
    cny: {
      type: Sequelize.STRING,
      field: 'cny'
    },
    emea_usd: {
      type: Sequelize.STRING,
      field: 'emea_usd'
    },
    usd_g: {
      type: Sequelize.STRING,
      field: 'usd_g'
    },
    //comments: {
    //  type: Sequelize.TEXT,
    //  field: 'comments',
    //  length: 'MEDIUM'
    //},
  },
  {
    tableName: 'product',
    freezeTableName: true,
    timestamps: false,
    underscored: true,
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  }
);

// Dev use only. {force: true} will drop the table if it already exists
// Product.sync({force: true});

// Production
// Product.sync();

// Product.removeAttribute('id');
export default Product;