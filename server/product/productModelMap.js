// Map the Schema column names to formatted display names
// isFilter describes whether the column is used to lookup
// products in the Product Filter component

// Unfortunately, there are two versions of this product map,
// one server-side and one client-side in initial state.
// They need to match unless or until server-side rendering is implemented.
let productModelMap = {
  "id": {
    name: "id",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "description": {
    name: "DESCRIPTION",
      isFilter: false,
      displayInRelatedProducts: true,
      position: 4,
  },
  "comments": {
    name: "COMMENTS",
    isFilter: false,
    displayInRelatedProducts: false,
    position: null,
  },
  "sku": {
    name: "SKU",
      isFilter: false,
      displayInRelatedProducts: true,
      position: 5,
  },
  "pack_price": {
    name: "PACK PRICE",
      isFilter: false,
      displayInRelatedProducts: true,
      position: 7,
  },
  "family": {
    name: "FAMILY",
      isFilter: true,
      displayInRelatedProducts: false,
      position: null,
  },
  "edition": {
    name: "EDITION",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 0,
  },
  "licensing_type": {
    name: "LIC TYPE",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 1,
  },
  "pack_size": {
    name: "PACK SIZE",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 2,
  },
  "sku_notes": {
    name: "SKU NOTES",
      isFilter: false,
      displayInRelatedProducts: true,
      position: 10,
  },
  "sku_type": {
    name: "SKU TYPE",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 3,
  },
  "premise_cloud": {
    name: "ON-PREMISE/ CLOUD",
    isFilter: true,
    displayInRelatedProducts: true,
    position: 9,
  },
  "sns_type": {
    name: "SnS TYPE",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 8,
  },
  "sns_length": {
    name: "SnS LENGTH",
      isFilter: true,
      displayInRelatedProducts: true,
      position: 9,
  },
  "sector": {
    name: "-C,-A,-F",
      isFilter: true,
      displayInRelatedProducts: false,
      position: null,
  },
  "eur": {
    name: "EUR",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "gbp": {
    name: "GBP",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "jpy": {
    name: "JPY",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "aud": {
    name: "AUD",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "cny": {
    name: "CNY",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "emea_usd": {
    name: "EMEA_USD",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "usd_g": {
    name: "USD_G",
      isFilter: false,
      displayInRelatedProducts: false,
      position: null,
  },
  "quantity": {
    name: "QUANTITY",
    isFilter: false,
    displayInRelatedProducts: false,
    position: null,
  },
  "total": {
    name: "TOTAL",
    isFilter: false,
    displayInRelatedProducts: false,
    position: null,
  }
};

export default productModelMap;