import {_} from 'lodash';
import Product from './productModel.js';
import productModelMap from './productModelMap.js';

// Create a data object to hold prepared data to send down to client
let data = {};

// Create a hash with sku's as keys for constant time
// lookup of product data in the SKU Search component
let skuHash = (productList) => {
  let hash = {};
  let cleanSku = "";
  _.forOwn(productList, (value, key) => {
    // TODO: match regex on back end with the search sku component regex
    cleanSku = (productList[key]["sku"]).replace(/-/g, "").toLowerCase();
    hash[cleanSku] = productList[key];
  });
  // console.log("sku hash test", hash["hz7a10aerca"]); // Test to see if hash works for a valid sku instance
  return hash;
};

// Get a simplified array of products
let getCleanProductArray = (rawProductList) => {
  let cleanProductArray = [];
  _.each(rawProductList, (product, i) => {
    cleanProductArray[i] = product["dataValues"];
  });
  return cleanProductArray;
};

/*  Use for reference in getUniqueOptionList function below.
 *  Example optionKey object structure:
     optionKey = {
       family :  {  //aka [select]
         optionsArray: [],   // aka optionKey[select]["optionsArray"]
         options: {
          HORIZON : HORIZON,  // aka optionKey[select]["options"][value]
          AIRWATCH : AIRWATCH,
          },
         productsFilteredByOption: {  // aka optionKey[select]["productsFilteredByOption][option]
            HORIZON: [],
            AIRWATCH: [],
         },
       edition : {
         optionsArray: {},
         options: {
          STANDARD: STANDARD,
         },
         productsFilteredByOption: {
          STANDARD: [],
         },
       }
     }
 */
let getUniqueOptionList = () => {
  let optionKey = {};
  // SELECT variable represents column names in the schema (e.g. Product Family, Product Edition, SKU, etc.)
  // OPTIONS variable represents unique drop-down options of a given select (e.g. Horizon, AirWatch, etc.)

  // Use a hash to get unique values for each drop-down menu in the Product Filter component
  _.each(data.cleanProductArray, (product, i) => {
    _.forOwn(product, (option, select) => {
      if (productModelMap[select]["isFilter"]) {
        // console.log("Here is the value of select: ", select, "and here is the value of option ", option);
        optionKey[select] = optionKey[select] || {};
        optionKey[select]["options"] = optionKey[select]["options"] || {};
        if (option !== "#REF!" && option !== "WORKSPACE ONE" && option !== "AIRWATCH") {
          optionKey[select]["options"][option] = option;
        }
      }
    });
  });

  // Create an array of unique options (for drop-down menu)
  // for each select
  _.forOwn(optionKey, (option, select) => {
    // create a blank array
    optionKey[select]["optionsArray"] = [];
    // console.log("this is the column options object: ", optionKey[select]["options"], "this is select ", select);
    // for each option in the select.options object
    _.forOwn(optionKey[select]["options"], (value, key) => {
      optionKey[select]["optionsArray"].push(value);
    })
  });

  // for each select in optionKey
  _.forOwn(optionKey, (value, select) => {
    optionKey[select]["productsFilteredByOption"] = {};
    // loop through the select's optionsArray and for every unique option
    _.each(value.optionsArray, (option) =>{
      // get an array of products where the option of that select matches
      optionKey[select]["productsFilteredByOption"][option] = _.filter(data.cleanProductArray, (product) => {
        return product[select] === option;
      });
    });
  });
  return optionKey;
};

let createData = (products, selectedCurrency) => {
  data.cleanProductArray = getCleanProductArray(products);
  data.skuHash = skuHash(data.cleanProductArray);
  data.optionKey =  getUniqueOptionList();
  data.productModelMap = productModelMap;
  data.selectedCurrency = selectedCurrency || 'pack_price';
  data.selected = {
    family: null,
    edition: null,
    licensing_type: null,
    pack_size: null,
    sku_type: null,
    premise_cloud: null,
    sns_type: null,
    sns_length: null,
    sector: null,
    sku: null,
  };
  data.current = {
    id: '',
    family: '',
    edition: '',
    licensing_type: '',
    description: '',
    comments: '',
    pack_size: '',
    pack_price: '',
    sku: '',
    sku_notes: '',
    sku_type: '',
    premise_cloud: '',
    sns_type: '',
    sns_length: '',
    sector: '',
    eur: '',
    gbp: '',
    jpy: '',
    aud: '',
    cny: '',
    emea_usd: '',
    usd_g: '',
    quantity: 1,
    total: 0,
  };

  return data;
};

let getInitialState = () => {
  Product.findAll()
    .then( products => {
      return createData(products);
    })
    .catch( error => {
      console.log("Initial state failed.");
      throw error;
    });
};

// Get all products, create some data structures for fast filtering and lookup on the client-side,
// then send back down to the client
let preparedData = (req, res) => {
  let selectedCurrency = req.body.selectedCurrency;
  Product.findAll()
    .then( products => {
      return createData(products, selectedCurrency);
    })
    .then( data => {
      res.status(202).json(data);
    })
    .catch( error => {
      console.log("preparedData function to get all products not working in prep.js");
      throw error;
    });
};

export {preparedData, getInitialState};
