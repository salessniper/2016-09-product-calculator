// IMPORTANT: Because there was not time to implement isomorphic loading,
// there are two places where state must be maintained. (Yep, it's not ideal.)
// If you make a change in this file, you also need to update prep.js.
// 1.
// The below is used for initial rendering of the page and is a placeholder
// until the ProductFilter can fetch the product list from the database and
// prepare the product data for rendering.
// 2.
// When data has come back from the database and onFetch is called in the ProductFilter,
// state gets updated with data from the server. State is built in the
// server/product/prep.js file so it should be updated whenever this file is updated.

// Example
export default {
  productData: {
    allProducts: [],
    current: {
      id: '',
      family: '',
      edition: '',
      licensing_type: '',
      description: '',
      // pack_size is also the default USD currency pack price
      pack_size: '',
      pack_price: '',
      sku: '',
      sku_notes: '',
      sku_type: '',
      sns_type: '',
      sns_length: '',
      sector: '',
      eur: '',
      gbp: '',
      jpy: '',
      aud: '',
      cny: '',
      emea_usd: '',
      usd_g: '',
      quantity: 1,
      total: 0,
    },
    searchedSku: null,
    necessaryDataIsProvidedToDisplayProductDescription: false,
    placeholder: "Enter a SKU",
    skuHash: null,
    cleanProductArray: [],
    currentProductArray: [],
    relatedProductArray: [],
    classNames: {

    },
    selectedCurrency: 'pack_price',
    selected: {
      family: null,
      edition: null,
      licensing_type: null,
      pack_size: null,
      sku_type: null,
      sns_type: null,
      sns_length: null,
      sector: null,
      sku: null,
    },
    productModelMap: {
      "id": {
        name: "id",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "description": {
        name: "DESCRIPTION",
        isFilter: false,
        displayInRelatedProducts: true,
        position: 4,
      },
      "comments": {
        name: "COMMENTS",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "sku": {
        name: "SKU",
        isFilter: false,
        displayInRelatedProducts: true,
        position: 5,
      },
      "pack_price": {
        name: "PACK PRICE",
        isFilter: false,
        displayInRelatedProducts: true,
        position: 7,
      },
      "family": {
        name: "FAMILY",
        isFilter: true,
        displayInRelatedProducts: false,
        position: null,
      },
      "edition": {
        name: "EDITION",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 0,
      },
      "licensing_type": {
        name: "LIC TYPE",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 1,
      },
      "pack_size": {
        name: "PACK SIZE",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 2,
      },
      "sku_notes": {
        name: "SKU NOTES",
        isFilter: false,
        displayInRelatedProducts: true,
        position: 10,
      },
      "sku_type": {
        name: "SKU TYPE",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 3,
      },
      "premise_cloud": {
        name: "ON-PREMISE/ CLOUD",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 9,
      },
      "sns_type": {
        name: "SnS TYPE",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 8,
      },
      "sns_length": {
        name: "SnS LENGTH",
        isFilter: true,
        displayInRelatedProducts: true,
        position: 9,
      },
      "sector": {
        name: "-C,-A,-F",
        isFilter: true,
        displayInRelatedProducts: false,
        position: null,
      },
      "eur": {
        name: "EUR",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "gbp": {
        name: "GBP",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "jpy": {
        name: "JPY",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "aud": {
        name: "AUD",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "cny": {
        name: "CNY",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "emea_usd": {
        name: "EMEA_USD",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "usd_g": {
        name: "USD_G",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "quantity": {
        name: "QUANTITY",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      },
      "total": {
        name: "TOTAL",
        isFilter: false,
        displayInRelatedProducts: false,
        position: null,
      }
    },
    optionKey: {
      family: {
        options: {
          HORIZON: 'HORIZON',
          THINAPP: 'THINAPP',
          'VSPHERE FOR DESKTOP': 'VSPHERE FOR DESKTOP'
        },
        optionsArray: ['HORIZON', 'THINAPP', 'VSPHERE FOR DESKTOP'],
      },
      edition: {
        options: {
          'HORIZON ADV ADD ON': 'HORIZON ADV ADD ON',
          'HORIZON ADV': 'HORIZON ADV',
          'HORIZON ENT': 'HORIZON ENT',
          'HORIZON ENT ADD ON': 'HORIZON ENT ADD ON',
          'HORIZON STD': 'HORIZON STD',
          'HORIZON STD ADD ON': 'HORIZON STD ADD ON',
          'THINAPP CLIENT': 'THINAPP CLIENT',
          'THINAPP SUITE (CLIENT + PACKAGER)': 'THINAPP SUITE (CLIENT + PACKAGER)',
          '': ''
        },
        optionsArray: ['HORIZON ADV ADD ON',
          'HORIZON ADV',
          'HORIZON ENT',
          'HORIZON ENT ADD ON',
          'HORIZON STD',
          'HORIZON STD ADD ON',
          'THINAPP CLIENT',
          'THINAPP SUITE (CLIENT + PACKAGER)',
          ''],
      },
      licensing_type: {
        options: {
          'CCU': 'CCU',
          'Named User': 'Named User',
          'Device or Named User': 'Device or Named User',
          'VM (CCU)': 'VM (CCU)'
        },
        optionsArray: ['CCU', 'Named User', 'Device or Named User', 'VM (CCU)'],
      },
      pack_size: {
        options: {
          '1': '1',
          '10': '10',
          '20': '20',
          '50': '50',
          '100': '100',
          '200': '200',
          '250': '250',
          '500': '500',
          '1000': '1000'
        },
        optionsArray: ['1', '10', '20', '50', '100', '200', '250', '500', '1000'],
      },
      sku_type: {
        options: {
          LIC: 'LIC',
          SNS: 'SNS',
          L1: 'L1',
          L2: 'L2',
          L3: 'L3',
          L4: 'L4',
          UPGRADE: 'UPGRADE',
          'L1 UPG': 'L1 UPG',
          'L2 UPG': 'L2 UPG',
          'L3 UPG': 'L3 UPG',
          'L4 UPG': 'L4 UPG'
        },
        optionsArray: ['LIC', 'SNS', 'L1', 'L2', 'L3', 'L4', 'UPGRADE', 'L1 UPG', 'L2 UPG', 'L3 UPG', 'L4 UPG'],
      },
      sns_type: {
        options: {
          'N/A': 'N/A',
          BASIC: 'BASIC',
          PRODUCTION: 'PRODUCTION'
        },
        optionsArray: ['N/A', 'BASIC', 'PRODUCTION'],
      },
      sns_length: {
        options: {
          'N/A': 'N/A',
          '1 Year': '1 Year',
          '3 Year': '3 Year',
          '2 Months': '2 Months'
        },
        optionsArray: ['N/A', '1 Year', '3 Year', '2 Months'],
      },
      sector: {
        options: {
          Academic: 'Academic',
          Commercial: 'Commercial'
        },
        optionsArray: ['Academic', 'Commercial'],
      },
    },
  }
};
