import { combineReducers } from 'redux';
import productData from './productReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
 productData,
 routing: routerReducer
});

export default rootReducer;
