import {
  UPDATE_CURRENT_PRODUCT,
  RESET_INITIAL_STATE,
  UPDATE_CURRENT_PRODUCT_ARRAY,
  UPDATE_PRODUCT_DESCRIPTION,
  UPDATE_SEARCHED_SKU,
  UPDATE_SELECTED,
  UPDATE_ALL_PRODUCT_INFO,
  UPDATE_OPTION_KEY,
  UPDATE_RELATED_PRODUCT_ARRAY,
  UPDATE_SELECTED_CURRENCY,
} from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function productReducer(state = initialState.productData, action) {
  let newState;

  switch (action.type) {
    case UPDATE_ALL_PRODUCT_INFO:
      return objectAssign({}, state, {
        skuHash: action.value.skuHash,
        optionKey: action.value.optionKey,
        cleanProductArray: action.value.cleanProductArray,
        selected: action.value.selected,
        selectedCurrency: action.value.selectedCurrency,
        // initialize the current product array to be all products, later it will be the current filtered list
        currentProductArray: action.value.cleanProductArray,
        productModelMap: action.value.productModelMap,
        current: action.value.current,
      });

    case RESET_INITIAL_STATE:
      // Yes, this IS a bad pattern, but the point here is to wipe out state entirely.
      return initialState.productData;

    case UPDATE_CURRENT_PRODUCT:
      return objectAssign({}, state, { current: action.value });

    case UPDATE_CURRENT_PRODUCT_ARRAY:
      return objectAssign({}, state, { currentProductArray: action.value });

    case UPDATE_SELECTED:
      return objectAssign({}, state, { selected: action.value});

    case UPDATE_RELATED_PRODUCT_ARRAY:
      return objectAssign({}, state, { relatedProductArray: action.value});

    case UPDATE_PRODUCT_DESCRIPTION:
      newState = objectAssign({}, state);
      // If you need to modify directly, get newState first, then modify:
      // newState.someExampleProperty = 9;
      return newState;

    case UPDATE_OPTION_KEY:
      return objectAssign({}, state, {optionKey: action.value});

    case UPDATE_SEARCHED_SKU:
      newState = objectAssign({}, state, { searchedSku: action.value });
      return newState;

    case UPDATE_SELECTED_CURRENCY:
      newState = objectAssign({}, state, { selectedCurrency: action.value });
      return newState;

    default:
      return state;
  }
}
