import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import UploadPage from './components/UploadPage';
import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="upload" component={UploadPage}/>
    <Route path="notFound" component={NotFoundPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);