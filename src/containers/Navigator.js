import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/navigatorActions';
import ProductFilter from '../components/ProductFilter';
import SkuSearch from '../components/SkuSearch.js';
import ProductDescription from '../components/ProductDescription';
import RelatedProducts from '../components/RelatedProducts';
import SetDefaults from '../components/SetDefaults';

export const Navigator = (props) => {
  return (
    <div className="navigator-container">
      <div className="flex-row">
        <ProductFilter
          updateRelatedProductArray={props.actions.updateRelatedProductArray}
          updateAllProductInfo={props.actions.updateAllProductInfo}
          resetInitialState={props.actions.resetInitialState}
          updateCurrentProduct={props.actions.updateCurrentProduct}
          updateCurrentProductArray={props.actions.updateCurrentProductArray}
          updateSelected={props.actions.updateSelected}
          updateProductDescription={props.actions.updateProductDescription}
          updateOptionKey={props.actions.updateOptionKey}
          productData={props.productData}
        />
        <div className="wrapper flex-col">
            <SkuSearch
              updateCurrentProduct={props.actions.updateCurrentProduct}
              updateRelatedProductArray={props.actions.updateRelatedProductArray}
              productData={props.productData}
              searchedSku={props.productData.searchedSku}
              updateSearchedSku={props.actions.updateSearchedSku}
            />
            <SetDefaults
              updateSelectedCurrency={props.actions.updateSelectedCurrency}
              productData={props.productData}
            />
        </div>
      </div>
      <div className="flex-row">
        <ProductDescription
          productData={props.productData}
        />
      </div>
      <div className="flex-row">
        <RelatedProducts
          productData={props.productData}
        />
      </div>
    </div>
  );
};

Navigator.propTypes = {
  actions: PropTypes.object.isRequired,
  productData: PropTypes.object.isRequired
};

let mapStateToProps = (state) => {
  return {
    productData: state.productData
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigator);
