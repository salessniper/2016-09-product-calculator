// helper functions used by multiple components

export function lookupBySku(sku) {
  return Object.assign(this.props.productData.current, this.props.productData["skuHash"][sku]);
}

export function getRelatedProductList(current) {
  return this.props.productData.cleanProductArray.filter( product => {
    return product['family'] === current['family'] &&
      product['edition'] === current['edition'] &&
      product['licensing_type'] === current['licensing_type'] &&
      product['pack_size'] === current['pack_size'] &&
      product['sector'] === current['sector'];
  });
}

export function getStrippedSku(sku) {
  return sku.replace(/[^a-zA-Z0-9]+/g,"").toLowerCase();
}

// once a unique sku has been located, ether with the sku search or filter component,
// update the currentProduct for display in the product description,
// and get a list of related products for that sku
export function onUniqueProductSelection(sku) {
  let strippedSku = this.getStrippedSku(sku);
  let currentProduct = this.lookupBySku(strippedSku);
  let relatedProducts = this.getRelatedProductList(currentProduct);
  this.props.updateRelatedProductArray(this.props.productData, relatedProducts);
  this.props.updateCurrentProduct(this.props.productData, 'allAttributes', currentProduct);
}