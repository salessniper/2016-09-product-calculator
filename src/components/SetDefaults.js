import React, {PropTypes} from 'react';
// import * as helpers from '../helpers/library.js';

// If other defaults are created, SetDefaults should become a container component
// and the currency radio button form should become one of its sub-components.
class SetDefaults extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
  }

  handleCurrencyChange(event) {
    this.props.updateSelectedCurrency(this.props.productData, event.target.value);
  }

  render() {
    const {productData} = this.props;

    return (
      <div className="component set-defaults">
        <div className="title-bar">
          <h2>Set Currency</h2>
        </div>
        <div>
          {/*<form onSubmit={this.handleFormSubmit}>*/}
          <form>
            <div className="radio">
              <label>
                <input type="radio" value="pack_price"
                       checked={productData.selectedCurrency === 'pack_price'}
                       onChange={this.handleCurrencyChange} />
                USD
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="eur"
                       checked={productData.selectedCurrency === 'eur'}
                       onChange={this.handleCurrencyChange} />
                EUR
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="gbp"
                       checked={productData.selectedCurrency === 'gbp'}
                       onChange={this.handleCurrencyChange} />
                GBP
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="jpy"
                       checked={productData.selectedCurrency === 'jpy'}
                       onChange={this.handleCurrencyChange} />
                JPY
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="aud"
                       checked={productData.selectedCurrency === 'aud'}
                       onChange={this.handleCurrencyChange} />
                AUD
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="cny"
                       checked={productData.selectedCurrency === 'cny'}
                       onChange={this.handleCurrencyChange} />
                CNY
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="emea_usd"
                       checked={productData.selectedCurrency === 'emea_usd'}
                       onChange={this.handleCurrencyChange} />
                EMEA_USD
              </label>
            </div>
            <div className="radio">
              <label>
                <input type="radio" value="usd_g"
                       checked={productData.selectedCurrency === 'usd_g'}
                       onChange={this.handleCurrencyChange} />
                USD_G
              </label>
            </div>
          </form>
          {/*<button type="submit">Save</button>*/}
        </div>
      </div>
    );
  }
}

SetDefaults.propTypes = {
  updateSelectedCurrency: PropTypes.func.isRequired,
  productData: PropTypes.object.isRequired,
};

export default SetDefaults;
