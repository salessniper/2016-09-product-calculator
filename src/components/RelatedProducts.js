import React, {PropTypes} from 'react';
import forOwn from 'lodash/forOwn';

class RelatedProducts extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onUpdateQuantity = this.onUpdateQuantity.bind(this);
  }

  onUpdateQuantity(name, value) {
    this.props.updateQuantity(this.props.productData, name, value);
  }

  render() {
    const seatPricePosition = 6;
    const packPricePosition = 7;
    const {productData} = this.props;
    let related = this.props.productData.relatedProductArray;
    let columnMap = [];
    let headers = [];
    let rows = [];
    let pMap = this.props.productData.productModelMap;

    // TODO: Refactor schema to include a calculated seat_price column
    // Manually add a seat price object to the product model map since it's not in the schema
    pMap.seat_price = {
      name: "SEAT PRICE",
      isFilter: false,
      displayInRelatedProducts: true,
      position: 6,
    };

    forOwn(pMap, (header, key) => {
      if (header["displayInRelatedProducts"]) {
        headers[header.position] = <th>{header.name}</th>;
        columnMap[header.position] = key;
      }
    });

    related.forEach(product => {
      let rowContents = [];
      columnMap.forEach((col, i) => {
        // get pack price formatting
        let pack_price = product[productData.selectedCurrency];
        let pack_price_string = pack_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        // if this is the seat price row, calculate the seat price from the pack price and push it
        if (i === seatPricePosition) {
          // TODO: refactor seat price calculation to tools/helpers instead of duplicating in prod description
          let seat_calc = (parseFloat(pack_price) / parseFloat(product["pack_size"])).toFixed(2);
          let seat_calc_string = seat_calc.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          let seat_price = isNaN(seat_calc) ? "" : seat_calc_string;
          rowContents.push(<td>{seat_price}</td>);
        // if this is the pack price row, display the formatted pack price
        } else if (i === packPricePosition) {
          rowContents.push(<td>{pack_price_string}</td>)
        // for all other rows, just push what's in the array
        } else {
          rowContents.push(<td className={col}>{product[col]}</td>);
        }
      });
      rows.push(<tr>{rowContents}</tr>);
      rowContents = [];
    });

    return (
      <div className="component related-products">
        <div className="title-bar">
          <h1>Related Products</h1>
        </div>
        <div className="attribute-container">
          <div>
            <table>
              <thead>
                {headers}
              </thead>
              <tbody>
                {rows}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

RelatedProducts.propTypes = {
  productData: PropTypes.object.isRequired
};

export default RelatedProducts;
