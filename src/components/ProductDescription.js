import React, {PropTypes} from 'react';
// import forOwn from 'lodash/forOwn';

class ProductDescription extends React.Component {
  constructor(props, context) {
    super(props, context);
    //let current = this.props.productData.current;
    this.onAddToQuote = this.onAddToQuote.bind(this);
    this.onUpdateQuantity = this.onUpdateQuantity.bind(this);
  }

  onAddToQuote(e) {
    this.props.addToQuote(this.props.productData, 'attributeValue', e.target.value);
  }

  onUpdateQuantity(name, value) {
    this.props.updateQuantity(this.props.productData, name, value);
  }

  render() {
    const {productData} = this.props;
    let current = productData.current;
    let pMap = productData.productModelMap;
    let pack_price = current[productData.selectedCurrency];
    let pack_price_string = pack_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let seat_calc = (parseFloat(pack_price) / parseFloat(current["pack_size"])).toFixed(2);
    let seat_calc_string = seat_calc.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let seat_price = isNaN(seat_calc) ? "" : seat_calc_string;

    return (
      <div className="component product-description">
        <div className="title-bar">
          <h1>Product Description</h1>
        </div>
        <div className="attribute-container">
          {/*<div>{createAttributes()}</div>*/}
          <div className="flex-row-left">
            <div className="flex-col one">
              <div className="wrapper">
                <h3>{current["edition"]}</h3>
                <div className="flex-row-desc">
                  <h5>{pMap["licensing_type"]["name"] + ": "}</h5>
                  <h5 className="green">{current["licensing_type"]}</h5>
                </div>
                <div className="flex-row-desc">
                  <h5>{pMap["pack_size"]["name"] + ": "} </h5>
                  <h5 className="green">{current["pack_size"]}</h5>
                </div>
                <div className="flex-row-desc">
                  <h5>{pMap["sku"]["name"] + ": "} </h5>
                  <h5 className="green">{current["sku"]}</h5>
                </div>
                <div className="flex-row-desc">
                  <h5>{pMap["sku_type"]["name"] + ": "} </h5>
                  <h5 className="green">{current["sku_type"]}</h5>
                </div>
              </div>
            </div>
            <div className="flex-col two">
              <div className="wrapper">
                <h5>{pMap["description"]["name"] + ":"}</h5>
                <p>{current["description"]}</p>
                <h5>{pMap["comments"]["name"] + ":"}</h5>
                <p>{current["comments"]}</p>
              </div>
            </div>
            <div className="flex-col three">
              <div className="wrapper">
                <div className="flex-row-desc">
                  <h5>{pMap["pack_price"]["name"] + ": "}</h5>
                  <h5 className="green">{pack_price_string}</h5>
                </div>
                <div className="flex-row-desc">
                  <h5>{"SEAT PRICE: "}</h5>
                  <h5 className="green">{seat_price}</h5>
                </div>
                  <div className="flex-row-desc">
                    <h5>{pMap["sns_type"]["name"] + ": "} </h5>
                    <h5 className="green">{current["sns_type"]}</h5>
                  </div>
                  <div className="flex-row-desc">
                    <h5>{pMap["sns_length"]["name"] + ": "} </h5>
                    <h5 className="green">{current["sns_length"]}</h5>
                  </div>
                  <div className="flex-row-desc">
                    <h5>{pMap["sector"]["name"] + ": "} </h5>
                    <h5 className="green">{current["sector"]}</h5>
                  </div>
              </div>
            </div>
          </div>
          <div className="flex-row-left sku_notes">
              <h5>{pMap["sku_notes"]["name"] + ": "}</h5>
              <p>{current["sku_notes"]}</p>
          </div>
        </div>
      </div>
    );
  }
}

ProductDescription.propTypes = {
  productData: PropTypes.object.isRequired
};

export default ProductDescription;
