import React from 'react';
import { Link, IndexLink } from 'react-router';

const Header = () => {
  return (
    <div className="header-container">
      <div className="header-content">
        <h1>Product Navigator</h1>
        <nav>
          <IndexLink to="/">Home</IndexLink>
          <Link to="/upload">Upload Page</Link>
          <Link to="/notFound">Not Found</Link>
        </nav>
      </div>
    </div>
  );
};

export default Header;


