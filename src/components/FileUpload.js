import React from 'react';
import 'isomorphic-fetch';
import 'es6-promise';

class FileUpload extends React.Component {
  constructor(props) {
    super(props);
    this.uploadFile = this.uploadFile.bind(this);
  }

  uploadFile(e) {
    let formData = new FormData();
    formData.append('file', this.refs.file.files[0]);
    fetch('http://127.0.0.1:8080/csv/master', {
      method: 'POST',
      body: formData
    })
    .then( response => {
      if (response.status === 202) {
        return response.json();
      } else if (response.status === 203) {
        // TODO: Consolidate; this is throwing a double alert when reverting to previous file.
        alert("There was a problem with your file format, so I had to revert to the previous file." +
          " Please fix the file formatting and try again.");
        return response.json();
      } else {
        throw "There was an error uploading. Please try again in a few minutes. If you see this message again, contact a site administrator.";
      }
    }).then( json => {
      alert("Your file was uploaded." + json[0]);
    }).catch( err =>{
      console.error("ERROR: data didn't get returned after the master csv file upload.", err);
      // alert("There was an error getting data back after the csv file upload. #2 ", err);
      throw err;
    });
    e.preventDefault();
  }

  render() {
    return (
      <div className="component upload">
        <form ref="uploadForm" method="get" encType="multipart/form-data" >
          <div className="flex-col">
            <span className="left">Select a CSV file for the <span className="text-highlight">Master&nbsp;</span>Product List:</span>
            <div className="flex-row-left">
              <input className="something submit" type="file" ref="file" name="fileupload" value="fileupload"></input>
              <input className="something submit" type="button" value="submit" onClick={this.uploadFile}/>
            </div>
          </div>
        </form>
        <h3>Instructions</h3>
        <ol>
          <li>1.  Save a copy of the file. Delete any columns not being used and make sure column order matches the below list exactly. Column names don't matter, just order. Make sure that your cell values are not referencing columns you deleted!</li>
          <li>Product Description, Comments, Part Number, Pack Price, Family, Edition, Licensing Type, Pack Size, SKU Notes, SKU Type, On Premise/Cloud, Sns Type, SnS Length, C/A/F, EUR, GBP, JPY, AUD, CNY, EMEA_USD, USD-G</li>
          <li>2.  Delete header rows. The first row should now be raw product data.</li>
          <li>3.  Delete any blank columns on the right of the sheet that may have formatting (e.g. borders/highlighting) on them.</li>
          <li>4.  Click to select all cells on the sheet and right click to Format Cells:</li>
            <ul className="indent">
              <li>-  Alignment Tab: Horizontal = Left (indent), Indent = 0, Wrap Text = Un-ticked</li>
              <li>-  Font Tab: Tick the Normal Font box</li>
              <li>-  Click OK to exit</li>
              <li>-  Under the Edit drop-down menu, hover over Find and then click Replace</li>
              <li>-  Find \n and replace with a space (don't type quotes around the space, just hit space bar.</li>
              <li>-  You may also want to do a Find and Replace on multiple spaces (eg 5 spaces in a row)</li>
            </ul>
          <li>5.  Save the file as a CSV</li>
          <li>6.  Open the file in Apache Open Office (import as UTF-8), Separated by Comma, Text Delimiter ". Note: the point of this step is to strip out bad characters from MS Excel that will interfere with importing the csv to the MySQL database.</li>
          <li>7.  Select all columns that contain pricing, then...</li>
          <li>8.  Choose:  Number, 2 decimal places and click OK.</li>
        </ol>
      </div>
    );
  }
}

FileUpload.propTypes = {
  // uploadFile: React.PropTypes.func.isRequired,
};

export default FileUpload;
