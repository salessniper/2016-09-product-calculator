import React, {PropTypes} from 'react';
import SkuTextInput from './SkuTextInput';
import * as helpers from '../helpers/library.js';

class SkuSearch extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.lookupBySku = helpers.lookupBySku.bind(this);
    this.getRelatedProductList = helpers.getRelatedProductList.bind(this);
    this.getStrippedSku = helpers.getStrippedSku.bind(this);
    this.onUniqueProductSelection = helpers.onUniqueProductSelection.bind(this);
  }

  handleChange(event) {
    this.props.updateSearchedSku(this.props.productData, 'searchedSku', event.target.value);
  }

  handleSubmit() {
    this.onUniqueProductSelection(this.props.searchedSku);
  }

  render() {
    const {productData} = this.props;

    return (
      <div className="component sku-search">
        <div className="title-bar">
          <h1>or Search by SKU</h1>
        </div>
        <div>
          <SkuTextInput
            className="sku-text-input"
            onChange={this.handleChange}
            name="skuSearchValue"
            placeholder="Enter a sku"
            value={productData.searchedSku}
            productData={productData}
          />
          <button onClick={this.handleSubmit}>Submit</button>
        </div>
      </div>
    );
  }
}

SkuSearch.propTypes = {
  updateRelatedProductArray: PropTypes.func.isRequired,
  updateCurrentProduct: PropTypes.func.isRequired,
  searchedSku: PropTypes.string.isRequired,
  productData: PropTypes.object.isRequired,
  updateSearchedSku: PropTypes.func.isRequired
};

export default SkuSearch;
