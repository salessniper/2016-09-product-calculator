import React from 'react';
import { Link } from 'react-router';

const NotFoundPage = () => {
  return (
    <div className="main-container">
      <div className="main-content">
        <h4>404 Page Not Found</h4>
        <Link to="/"> Go back to homepage </Link>
      </div>
    </div>
  );
};

export default NotFoundPage;