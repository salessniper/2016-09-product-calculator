import React from 'react';
// import {Link} from 'react-router';
import FileUpload from './FileUpload';

const UploadPage = () => {
  return (
    <div className="main-container">
      <div className="main-content">
        <div className="layout-container">
          <div className="upload">
            <div className="flex-row">
              <div className="component upload">
                <div className="title-bar">
                  <h1>Upload Files</h1>
                </div>
                <div>
                  <FileUpload/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UploadPage;