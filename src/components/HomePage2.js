import React from 'react';
// import {Link} from 'react-router';

const HomePage = () => {
  return (
    <div className="main-container">
      <div className="main-content">
        <div className="layout-container">
          <div className="selector">
            <div className="title-bar">
              <h1>Select a Product</h1>
            </div>
            <div className="flex-row">
              <div className="component dropdown-selector">
                <p>This is the dropdown selector</p>
              </div>
              <div className="flex-col">
                <div className="component lookup-sku">
                  <p>This is the SKU Lookup</p>
                </div>
                <div className="component lookup-upgrade">
                  <p>This is the upgrade lookup</p>
                </div>
              </div>
            </div>
          </div>
          <div className="product-description">
            <div className="title-bar">
              <h1>Product Description</h1>
            </div>
            <div className="component description">
              <p>This is the product description</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;