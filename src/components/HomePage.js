import React from 'react';
// import {Link} from 'react-router';
import Navigator from '../containers/Navigator'; // eslint-disable-line import/no-named-as-default


const HomePage = () => {

  return (
    <div className="main-container">
      <div className="main-content">
        <div className="layout-container">
          <Navigator/>
        </div>
      </div>
    </div>
  );
};



export default HomePage;