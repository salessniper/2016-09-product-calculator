import React, {PropTypes} from 'react';
import {forOwn} from 'lodash';
import {each} from 'lodash';
import {uniqBy} from 'lodash';
import initialState from '../reducers/initialState.js';
import * as helpers from '../helpers/library.js';
// classnames allows you to use true/false values to toggle classes for styling purposes
// import classNames from 'classnames';

class ProductFilter extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onAttributeSelection = this.onAttributeSelection.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getNewProductObject = this.getNewProductObject.bind(this);
    this.getNewCurrentProductArray = this.getNewCurrentProductArray.bind(this);
    this.getNewOptionKey = this.getNewOptionKey.bind(this);
    this.onSkuSelection = this.onSkuSelection.bind(this);
    this.fetchProductData = this.fetchProductData.bind(this);
    this.getStrippedSku = helpers.getStrippedSku.bind(this);
    this.lookupBySku = helpers.lookupBySku.bind(this);
    this.getRelatedProductList = helpers.getRelatedProductList.bind(this);
    this.onUniqueProductSelection = helpers.onUniqueProductSelection.bind(this);
  }

  // Do a database call just before rendering to get all products
  // and related data structures.
  componentWillMount() {
    this.fetchProductData();
  }

  handleReset() {
    // while resetInitialState isn't strictly necessary, it makes the page appear to update noticeably faster
    let data = initialState.productData;
    data.selectedCurrency = this.props.productData.selectedCurrency;
    this.props.resetInitialState(this.props.productData, data);
    // fetch product data is getting state from the server product model and merging with client-side initial state
    // this is obviously not ideal because there are two versions of the product model map but saves
    // having to implement server-side rendering and allows us to build out more features in the short run
    this.fetchProductData();
  }

  fetchProductData() {
    fetch('http://127.0.0.1:8080/product/prep', {
      //method: 'GET'
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ selectedCurrency: this.props.productData.selectedCurrency})
    })
    .then( response => {
      if (response.status === 202) {
        return response.json();
      } else {
        alert("Data didn't come down from server.");
        throw "Oops!";
      }
    }).then( data => {
      // alert("Here's the data that came back from prep.  " + data.skuHash.vcanprehazdoc.price_US);
      // console.log("option key in will mount", data.optionKey);
      this.onFetch(data);

    }).catch( err =>{
      alert("Failed to get data back from prep. ", err);
      throw err;
    });
  }

  getNewOptionKey() {
    // create a new optionKey object
    let optionKey = {};
    let productData = this.props.productData;
    let currentProductArray = productData.currentProductArray;
    // console.log("current product array in get new option key ", currentProductArray);
    // SELECT variable represents column names in the schema (e.g. Product Family, Product Edition, SKU, etc.)
    // OPTIONS variable represents unique drop-down options of a given select (e.g. Horizon, AirWatch, etc.)

    // Use a hash to get unique values for each drop-down menu in the Product Filter component
    each(currentProductArray, (product) => {
      forOwn(product, (option, select) => {
        if (productData.productModelMap[select]["isFilter"]) {
          // console.log("Here is the value of select: ", select, "and here is the value of option ", option);
          optionKey[select] = optionKey[select] || {};
          optionKey[select]["options"] = optionKey[select]["options"] || {};
          optionKey[select]["options"][option] = option;
        }
      });
    });

    // Create an array of unique options (for drop-down menu)
    // for each select
    forOwn(optionKey, (option, select) => {
      // create a blank array
      optionKey[select]["optionsArray"] = [];
      // console.log("this is the column options object: ", optionKey[select]["options"], "this is select ", select);
      // for each option in the select.options object
      forOwn(optionKey[select]["options"], (value) => {
        optionKey[select]["optionsArray"].push(value);
      })
    });
    // console.log("option key in the product filter page", optionKey);
    // console.log("example array from filteredbyoption", optionKey["edition"]["productsFilteredByOption"]["HORIZON ADV"][0])
    return optionKey;
  }

  getNewProductObject(input, name, value) {
    let obj = {};
    obj[name] = value;
    return Object.assign(input, obj);
  }

  getNewCurrentProductArray(name, value) {
    let filteredProducts = this.props.productData.currentProductArray || this.props.productData.cleanProductArray;
    let filtered = filteredProducts.filter( (item) => {
      return item[name] === value;
    });
    return uniqBy(filtered, "sku");
  }

  onFetch(data) {
    this.props.updateAllProductInfo(this.props.productData, "allData", data);
  }

  // When an option is selected:
  onAttributeSelection(e) {
    let name = e.target.name;
    let value = e.target.value;
    // Set the productData.selected to reflect the new choice made by the user
    let p1 = Promise.resolve( () => {
      return this.getNewProductObject(this.props.productData.selected, name, value);
    });
    p1.then(newSelected => {
      newSelected = newSelected();
      // console.log("new current selected list: ", newSelected);
      return this.props.updateSelected(this.props.productData, name, newSelected);
    // Create a new array of current products based on the new selection
    }).then(() => {
      return this.getNewCurrentProductArray(name, value);
    }).then(newCurrentProductArray => {
      if (newCurrentProductArray.length === 1) {
        this.onUniqueProductSelection(newCurrentProductArray[0]["sku"]);
      }
      return this.props.updateCurrentProductArray(this.props.productData, name, newCurrentProductArray);
    // Set the optionKeys using the current product array
    }).then(() => {
      return Object.assign(this.props.productData.optionKey, this.getNewOptionKey());
    // When optionKeys update the product filter will re-render and show new options
    }).then(newOptionKey => {
      this.props.updateOptionKey(this.props.productData, name, newOptionKey);
    })
    .catch(err => {
      console.log("There was an error in onAttributeSelection in the Product Filter: ", err);
    });
  }

  onSkuSelection(e) {
    this.onUniqueProductSelection(e.target.value);
  }

  render() {
    const {productData} = this.props;
    // console.log("Product data on render: ", productData);

    // Dynamically map drop-down options for selects
    let createFilters = () => {
      let defaultSelectArray = [];
      let skuArray = [];
      let filter;
      forOwn(productData.optionKey, (value, defaultSelect) => {
        let displayDefaultSelect = productData["productModelMap"][defaultSelect]["name"];
        filter = productData["optionKey"][defaultSelect]["optionsArray"].map( (option) =>
          <option ref={option} key={option} value={option}>{option}</option>
        );
        filter.unshift(
          <option ref={defaultSelect} value={displayDefaultSelect} disabled>{displayDefaultSelect}</option>
        );
        // only push the select element if there are non-default options to choose from
        if (filter.length === 2) {
          defaultSelectArray.push(
            <select
              className="activated"
              name={defaultSelect}
              onChange={this.onAttributeSelection}
              value={filter[1]["props"]["value"]}
            >
              {filter}
            </select>
          );
        } else if (filter.length > 2) {
          defaultSelectArray.push(
            <select
              name={defaultSelect}
              onChange={this.onAttributeSelection}
              value={productData["selected"][defaultSelect] || displayDefaultSelect}
            >
              {filter}
            </select>
          );
        } else {
          // if a filter has 0 options don't display it at all
        }

      });

      // Once choices have narrowed down the sku list to a manageable size,
      // display a drop-down with possible sku choices
      if (productData.selected["family"] && productData.currentProductArray.length < 20) {
        let currentSku;
        let selectedSku;
        productData.currentProductArray.forEach((product, i) => {
          currentSku = productData.currentProductArray[i]["sku"];
          skuArray.push(<option key={currentSku} value={currentSku}>{currentSku}</option>);
        });
        skuArray.unshift(
          <option value="SKU" disabled onClick={this.unSelect}>SKU</option>
        );

        selectedSku = skuArray[1]["props"]["value"];
        if (skuArray.length === 2) {
          defaultSelectArray.push(
            <select
              className="activated" name="sku" onChange={this.onSkuSelection} value={selectedSku}>
              {skuArray}
            </select>
          );
        } else if (skuArray.length > 2) {
          defaultSelectArray.push(
            <select name="sku" onChange={this.onSkuSelection} value="SKU">
              {skuArray}
            </select>
          );
        }
      } else {
        // console.log("Still too many SKU's to display.");
      }
      return defaultSelectArray;
    };

    return (
      <div className="component product-filter">
        <div className="title-bar">

            <h1>Product Filter</h1>
            <button onClick={this.handleReset}>Reset</button>

        </div>
        <div className="filter-bars flex-col">
          <div>{createFilters()}</div>
        </div>
      </div>
    );
  }
}

ProductFilter.propTypes = {
  updateRelatedProductArray: PropTypes.func.isRequired,
  updateAllProductInfo: PropTypes.func.isRequired,
  updateProductDescription: PropTypes.func.isRequired,
  updateSelected: PropTypes.func.isRequired,
  updateCurrentProduct: PropTypes.func.isRequired,
  updateCurrentProductArray: PropTypes.func.isRequired,
  updateOptionKey: PropTypes.func.isRequired,
  productData: PropTypes.object.isRequired,
  resetInitialState: PropTypes.func.isRequired,
};

export default ProductFilter;
