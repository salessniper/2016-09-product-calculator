import React, {PropTypes} from 'react';
// TODO: Implement this for  SKU filter text input
const SkuTextInput = (props) => {
  //const handleChange = (event) => {
  //  console.log(props.placeholder);
  //  console.log("sku text input---", event.target.value);
  //  // props.onChange(props.name, e.target.value);
  //};

  return (
    <input
      type="text"
      className={props.className}
      placeholder={props.placeholder}
      value={props.value}
      onChange={props.onChange}
    />
  );
};

SkuTextInput.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  productData: PropTypes.object.isRequired

};

export default SkuTextInput;
