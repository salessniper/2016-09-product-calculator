import * as types from '../constants/actionTypes';
// import dateHelper from '../utils/dateHelper';

// example of a thunk using the redux-thunk middleware
// export function saveCurrentProduct(settings) {
//  return function (dispatch) {
//    // thunks allow for pre-processing actions, calling apis, and dispatching multiple actions
//    // in this case at this point we could call a service that would persist the data
//    return dispatch({
//      type: types.SAVE_CURRENT_PRODUCT,
//      dateModified: dateHelper.getFormattedDateTime(),
//      settings
//    });
//  };
// }

export function updateAllProductInfo(settings, name, value) {
  return {
    type: types.UPDATE_ALL_PRODUCT_INFO,
    settings,
    name,
    value
  };
}

export function resetInitialState(settings, value) {
  return {
    type: types.RESET_INITIAL_STATE,
    settings,
    value
  };
}

export function updateCurrentProduct(settings, name, value) {
  return {
    type: types.UPDATE_CURRENT_PRODUCT,
    // dateModified: dateHelper.getFormattedDateTime(),
    settings,
    name,
    value
  };
}

export function updateCurrentProductArray(settings, name, value) {
  return {
    type: types.UPDATE_CURRENT_PRODUCT_ARRAY,
    settings,
    name,
    value
  };
}

export function updateSelected(settings, name, value) {
  return {
    type: types.UPDATE_SELECTED,
    settings,
    name,
    value
  };
}

export function updateOptionKey(settings, name, value) {
  return {
    type: types.UPDATE_OPTION_KEY,
    settings,
    name,
    value
  };
}

export function updateRelatedProductArray(settings, value) {
  return {
    type: types.UPDATE_RELATED_PRODUCT_ARRAY,
    settings,
    value,
  };
}

export function updateProductDescription(settings, name, value) {
  return {
    type: types.UPDATE_PRODUCT_DESCRIPTION,
    settings,
    name,
    value
  };
}

export function updateSearchedSku(settings, name, value) {
  return {
    type: types.UPDATE_SEARCHED_SKU,
    settings,
    name,
    value
  };
}

export function updateSelectedCurrency(settings, value) {
  return {
    type: types.UPDATE_SELECTED_CURRENCY,
    value,
  }
}
