Kevin here, I figured out how to run the project in any cloud, despite several small issues here and there. In my case, I ran it on an AWS EC2 instance.
 
I’ll be listing the requirements needed, the steps to reproduce, and then the bugs and quirks to expect, since they are also seen on vmw-sku-navigator.com
 
# Requirements:

-          an EC2 instance with the following:
o    VMWare SKU project code base
o    Node.js v4 and above
o    A connection to MySQL. In my case, it was a MySql instance on localhost, with user ‘root’ and a database named “vmw”. Expect a table to be created, called “product”.
-          A note of what the user/password is for the database
-          A CSV to initialize the database. In my case it’s the master-*.csv file attached in this email. Without the database initially having data, other things break, like the web app’s csv upload feature.
 
# Steps to reproduce:

1.       cd into the project directory, we’ll call it $PROJECT for now
2.       `npm i` to install dependencies into node_modules/
3.       Create a directory called uploads:

```
mkdir –p ./server/csv/uploads
```
 
4.       If it doesn’t exist due to .gitignore, create a file in `$PROJECT/server/config/mysqlsetup.js` with the content:
 
```
const password = "";
export default password;
```

This should be the password to the database. The rest of the db credentials are in $PROJECT/server/database/db.js
 
5.       In `$PROJECT/server/product/productModel.js` add these + lines near the top of the file:

```
var Product = db.define('product',
   {
+    id: {
+      type: Sequelize.INTEGER,
+      field: 'id',
+      primaryKey: true,
+      autoIncrement: true
+    },
     description: {
       type: Sequelize.TEXT,
       field: 'description',
```      
 
6.       In `$PROJECT/src/styles/styles.scss` add in these lines near the bottom of the file:

 
```
...  }
}
 
+div.radio {
+  label {
+    color: black;
+  }
+}
/////////////////// Other Styles ///////////////////////////
.text-highlight {
   color: $vm-blue;
}
```

7.       `npm run build` to start the server. This will build the source code, followed by spinning up a server on port 8080.
Keep this server running during the next steps.
 
8.       Each time the server starts, it CREATEs the SQL database table `product`. The creation in this is a little broken because it’s missing the “id” INT Primary Key. Add it with this SQL script:

```
ALTER TABLE `vmw`.`product`
ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT AFTER `usd_g`,
ADD PRIMARY KEY (`id`);
```
 
9.       While the server is still running, populate the database with either of the attached CSVs. In my case, the .csv file was local to the mysql server so I could populate it like so:
 
```
LOAD DATA LOCAL INFILE '/home/ec2-user/2016-09-product-calculator/server/csv/uploads/master-v5.03-oo-ready.csv' 
INTO TABLE `vmw`.`product` 
COLUMNS TERMINATED BY ',' ENCLOSED BY '\"' 
LINES TERMINATED BY '\n';
```
 
10.   Make sure that the “vmw”.”product” table still has some rows, as well as the “id” column. Otherwise you get the following error:

```
Unhandled rejection SequelizeBaseError: ER_BAD_FIELD_ERROR: Unknown column 'id' in 'field list'
```

11.   You can now visit the site through it’s public IP address or DNS, on port 8080. This all depends on how it’s configured with AWS, but what I did was add a Security Group to the EC2 instance that allows TCP connections to port 8080.
 
# Ongoing bugs/quirks:
 
-          Each time the server is set up with the command “npm run build”, the “product” table is deleted and re-created. This is why the “id” column and the db need to be populated after each run.
-          In the “src/” folder, there are 2 places where there are requests to “127.0.0.1/something”. Those requests need to be updated to “/something” instead, otherwise this won’t work in a production environment. This is partly why the app only works locally.
-          The “npm run start” script doesn’t work for starting up the server. The only one that works is “npm run build”.
-          There is a MySQL setting preventing from doing uploads via the web app, and I believe it’s due to a setting called `secure_file_priv`. Make sure secure_file_priv=”” in the MySQL configuration file. In EC2, the file was in `/etc/my.cnf`
-          Under the Related Products section, in all my testing, all the related products ended up being the same product. *this has been fixed in Pull Request #2*.
-          One thing that still needs to get done is turning the node process into a web service. Nodemon is already in place for this, but I also recommend using a tool like pm2
 




# React Slingshot!

[![Build status: Linux](https://img.shields.io/travis/coryhouse/react-slingshot.svg?style=flat-square)](https://travis-ci.org/coryhouse/react-slingshot)
[![Build status: Windows](https://ci.appveyor.com/api/projects/status/ky0npqkot20ieiak?svg=true)](https://ci.appveyor.com/project/coryhouse/react-slingshot/branch/master)
[![Dependency Status](https://david-dm.org/coryhouse/react-slingshot.svg?style=flat-square)](https://david-dm.org/coryhouse/react-slingshot)
[![Coverage Status](https://coveralls.io/repos/github/coryhouse/react-slingshot/badge.svg?branch=master)](https://coveralls.io/github/coryhouse/react-slingshot?branch=master)

React Slingshot is a comprehensive starter kit for rapid application development using React. 

Why Slingshot?

1. **One command to get started** - Type `npm start` to start development in your default browser.
2. **Rapid feedback** - Each time you hit save, changes hot reload and linting and automated tests run.
3. **One command line to check** - All feedback is displayed on a single command line.
4. **No more JavaScript fatigue** - Slingshot uses the most popular and powerful libraries for working with React.
5. **Working example app** - The included example app shows how this all works together.
6. **Automated production build** - Type `npm run build` to do all this:

[![React Slingshot Production Build](https://img.youtube.com/vi/qlfDLsX-J0U/0.jpg)](https://www.youtube.com/watch?v=qlfDLsX-J0U)

## Get Started
1. **Initial Machine Setup**. First time running the starter kit? Then complete the [Initial Machine Setup](https://github.com/coryhouse/react-slingshot#initial-machine-setup).
2. **Clone the project**. `git clone https://github.com/coryhouse/react-slingshot.git`.
3. **Run the setup script**. `npm run setup`
4. **Run the example app**. `npm start -s`
This will run the automated build process, start up a webserver, and open the application in your default browser. When doing development with this kit, this command will continue watching all your files. Every time you hit save the code is rebuilt, linting runs, and tests run automatically. Note: The -s flag is optional. It enables silent mode which suppresses unnecessary messages during the build.
5. **Review the example app.** This starter kit includes a working example app that calculates fuel savings. Note how all source code is placed under /src. Tests are placed alongside the file under test. The final built app is placed under /dist. These are the files you run in production.
6. **Delete the example app files.** Once you're comfortable with how the example app works, you can [delete those files and begin creating your own app](https://github.com/coryhouse/react-slingshot/blob/master/docs/FAQ.md#i-just-want-an-empty-starter-kit).

##Initial Machine Setup
1. **Install [Node 4.0.0 or greater](https://nodejs.org)** - (5.0 or greater is recommended for optimal build performance). Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm).
2. **Install [Git](https://git-scm.com/downloads)**. 
3. On a Mac? You're all set. If you're on Linux or Windows, complete the steps for your OS below.  
 
**On Linux:**  

 * Run this to [increase the limit](http://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc) on the number of files Linux will watch. [Here's why](https://github.com/coryhouse/react-slingshot/issues/6).    
`echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p` 

**On Windows:** 
 
* **Install [Python 2.7](https://www.python.org/downloads/)**. Some node modules may rely on node-gyp, which requires Python on Windows.
* **Install C++ Compiler**. Browser-sync requires a C++ compiler on Windows. [Visual Studio Express](https://www.visualstudio.com/en-US/products/visual-studio-express-vs) comes bundled with a free C++ compiler. Or, if you already have Visual Studio installed: Open Visual Studio and go to File -> New -> Project -> Visual C++ -> Install Visual C++ Tools for Windows Desktop. The C++ compiler is used to compile browser-sync (and perhaps other Node modules).

##Technologies
Slingshot offers a rich development experience using the following technologies:

| **Tech** | **Description** |**Learn More**|
|----------|-------|---|
|  [React](https://facebook.github.io/react/)  |   Fast, composable client-side components.    | [Pluralsight Course](https://www.pluralsight.com/courses/react-flux-building-applications)  |
|  [Redux](http://redux.js.org) |  Enforces unidirectional data flows and immutable, hot reloadable store. Supports time-travel debugging. Lean alternative to [Facebook's Flux](https://facebook.github.io/flux/docs/overview.html).| [Pluralsight Course](http://www.pluralsight.com/courses/react-redux-react-router-es6)    |
|  [React Router](https://github.com/reactjs/react-router) | A complete routing library for React | [Pluralsight Course](https://www.pluralsight.com/courses/react-flux-building-applications) |
|  [Babel](http://babeljs.io) |  Compiles ES6 to ES5. Enjoy the new version of JavaScript today.     | [ES6 REPL](https://babeljs.io/repl/), [ES6 vs ES5](http://es6-features.org), [ES6 Katas](http://es6katas.org), [Pluralsight course](https://www.pluralsight.com/courses/javascript-fundamentals-es6)    |
| [Webpack](http://webpack.github.io) | Bundles npm packages and our JS into a single file. Includes hot reloading via [react-transform-hmr](https://www.npmjs.com/package/react-transform-hmr). | [Quick Webpack How-to](https://github.com/petehunt/webpack-howto) [Pluralsight Course](https://www.pluralsight.com/courses/webpack-fundamentals)|
| [Browsersync](https://www.browsersync.io/) | Lightweight development HTTP server that supports synchronized testing and debugging on multiple devices. | [Intro vid](https://www.youtube.com/watch?time_continue=1&v=heNWfzc7ufQ)|
| [Mocha](http://mochajs.org) | Automated tests with [Chai](http://chaijs.com/) for assertions and [Enzyme](https://github.com/airbnb/enzyme) for DOM testing without a browser using Node. | [Pluralsight Course](https://www.pluralsight.com/courses/testing-javascript) |
| [Isparta](https://github.com/douglasduteil/isparta) | Code coverage tool for ES6 code transpiled by Babel. | 
| [TrackJS](https://trackjs.com/) | JavaScript error tracking. | [Free trial](https://my.trackjs.com/signup)|  
| [ESLint](http://eslint.org/)| Lint JS. Reports syntax and style issues. Using [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react) for additional React specific linting rules. | |
| [SASS](http://sass-lang.com/) | Compiled CSS styles with variables, functions, and more. | [Pluralsight Course](https://www.pluralsight.com/courses/better-css)|
| [PostCSS](https://github.com/postcss/postcss) | Transform styles with JS plugins. Used to autoprefix CSS |
| [Editor Config](http://editorconfig.org) | Enforce consistent editor settings (spaces vs tabs, etc). | [IDE Plugins](http://editorconfig.org/#download) |
| [npm Scripts](https://docs.npmjs.com/misc/scripts)| Glues all this together in a handy automated build. | [Pluralsight course](https://www.pluralsight.com/courses/npm-build-tool-introduction), [Why not Gulp?](https://medium.com/@housecor/why-i-left-gulp-and-grunt-for-npm-scripts-3d6853dd22b8#.vtaziro8n)  |

The starter kit includes a working example app that puts all of the above to use.
## Questions?
Check out the [FAQ](/docs/FAQ.md)
